import GateSystem from '@/gatesystem/GateSystem';

describe('testing GateSystem class:', () => {
  const stateMock = jest.fn(() => ({}))() as any;
  let gateSystem = new GateSystem();

  afterAll(() => {
    gateSystem = new GateSystem();
  });

  describe('setState:', () => {
    const setStateSpy = jest.spyOn(gateSystem, 'setState');
    const notifySpy = jest.spyOn(gateSystem.eventManager, 'notify');

    beforeEach(() => {
      gateSystem.setState(stateMock);
    });

    afterEach(() => {
      setStateSpy.mockClear();
      notifySpy.mockClear();
    });

    test('should be called', () => {
      expect(setStateSpy).toHaveBeenCalled();
      expect(notifySpy).toHaveBeenCalled();
    });
  });

  describe('handleMainButtonPress:', () => {
    test('should be called', () => {
      stateMock.handleMainButtonPress = jest.fn();
      gateSystem.setState(stateMock);
      gateSystem.handleMainButtonPress();

      expect(stateMock.handleMainButtonPress).toHaveBeenCalled();
    });
  });

  describe('testing closingDelay:', () => {
    test('should set new delay', () => {
      const newClosingDelay = 5000;
      gateSystem.setClosingDelay(newClosingDelay);

      expect(gateSystem.getClosingDelay()).toEqual(newClosingDelay);
    });
  });

  describe('testing movementTime:', () => {
    test('should set new delay', () => {
      const newMovementTime = 5000;
      gateSystem.setMovementTime(newMovementTime);

      expect(gateSystem.getMovementTime()).toEqual(newMovementTime);
    });
  });
});
