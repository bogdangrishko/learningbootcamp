import OpeningState from '@/gatesystem/OpeningState'; // eslint-disable-line

class OpenedStateMock {
  a = 1;

  b = 1;
}
class ClosingStateMock {
  a = 2;

  b = 2;
}

const gateSystemMock = {
  states: {
    OpenedState: OpenedStateMock,
    ClosingState: ClosingStateMock,
  },
  setState: jest.fn(),
  getMovementTime: jest.fn(),
} as any;

describe('OpeningState', () => {
  let instance: OpeningState;

  beforeEach(() => {
    instance = new OpeningState(gateSystemMock);
  });

  describe('#setNextState', () => {
    it('should set OpenedState state', () => {
      instance.setNextState();

      expect(gateSystemMock.setState).toHaveBeenCalledWith(new OpenedStateMock());
    });
  });

  describe('#setOppositeState', () => {
    it('should set ClosingState state', () => {
      instance.setOppositeState();

      expect(gateSystemMock.setState).toHaveBeenCalledWith(new ClosingStateMock());
    });
  });
});
