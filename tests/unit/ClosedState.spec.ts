import ClosedState from '@/gatesystem/ClosedState';

describe('testing ClosedState class:', () => {
  class OpeningStateMock {}

  const gateSystemMock = {
    states: {
      OpeningState: OpeningStateMock,
    },
    setState: jest.fn(),
  } as any;

  let closedState = new ClosedState(gateSystemMock);

  afterAll(() => {
    closedState = new ClosedState(gateSystemMock);
  });

  test('handleMainButtonPress should be called', () => {
    const handleMainButtonPressSpy = jest.spyOn(closedState, 'handleMainButtonPress');

    closedState.handleMainButtonPress();

    expect(handleMainButtonPressSpy).toHaveBeenCalled();

    handleMainButtonPressSpy.mockClear();
  });
});
