import OpenedState from '@/gatesystem/OpenedState';
import GateSystem from '@/gatesystem/GateSystem';

let gateSystem: any; let
  openedState: any;

beforeEach(() => {
  jest.resetAllMocks();

  jest.spyOn(OpenedState.prototype, 'initClosingTimer');
  jest.spyOn(OpenedState.prototype, 'resetClosingTimer');
  jest.spyOn(GateSystem.prototype, 'setState');

  gateSystem = new GateSystem();
});

describe('OpenedState', () => {
  beforeEach(() => {
    openedState = new OpenedState(gateSystem);
  });

  it('should call `initClosingTimer` one times', () => {
    expect(openedState.initClosingTimer).toHaveBeenCalledTimes(1);
  });

  it('should call `resetClosingTimer` one time when `handleMainButtonPress` is called', () => {
    openedState.handleMainButtonPress();
    expect(openedState.resetClosingTimer).toHaveBeenCalledTimes(1);
  });

  it('should call `setState` one time when `handleMainButtonPress` is called', () => {
    openedState.handleMainButtonPress();
    expect(gateSystem.setState).toHaveBeenCalledTimes(1);
  });
});

describe('OpenedState when sensor see some car', () => {
  beforeEach(() => {
    gateSystem.sensor.hasCar = jest.fn().mockReturnValue(true);
    openedState = new OpenedState(gateSystem);
  });

  it('should call `initClosingTimer` two times when `handleMainButtonPress` is called', () => {
    openedState.handleMainButtonPress();
    expect(openedState.initClosingTimer).toHaveBeenCalledTimes(2);
  });
});
