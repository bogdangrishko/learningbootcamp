import ClosingState from '@/gatesystem/ClosingState';

describe('ClosingState', () => {
  let closingState: any; let sensorMock: any; let
    gateSystemMock: any;

  beforeEach(() => {
    jest.useFakeTimers();

    sensorMock = {
      attach: jest.fn(),
      detach: jest.fn(),
    };

    gateSystemMock = {
      sensor: sensorMock,

      states: {
        ClosedState: jest.fn().mockImplementation(() => ({})),
        OpeningState: jest.fn().mockImplementation(() => ({})),
      },

      getMovementTime: jest.fn(() => 10000),
      getClosingDelay: jest.fn(() => 10000),
      setState: jest.fn(),
    };

    closingState = new ClosingState(gateSystemMock as any);
  });

  test('ClosingState should watch Sensor', () => {
    expect(sensorMock.attach).toHaveBeenCalledWith(closingState);
  });

  test('ClosingState should automatically changed to ClosedState', () => {
    jest.runOnlyPendingTimers();

    expect(gateSystemMock.setState).toHaveBeenCalled();
    expect(gateSystemMock.states.ClosedState).toHaveBeenCalled();
  });

  test('ClosingState should be changed to OpeningState after second button press', () => {
    closingState.handleMainButtonPress();
    closingState.handleMainButtonPress();

    expect(gateSystemMock.setState).toHaveBeenCalled();
    expect(gateSystemMock.states.OpeningState).toHaveBeenCalled();
  });
});
