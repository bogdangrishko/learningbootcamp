import { Subject as SubjectType, Observer, GateState } from '@/gatesystem/types';

export default abstract class Subject implements SubjectType {
  protected observers: Observer[] = [];

  public attach(observer: Observer): void {
    const observerIndex: number = this.observers.indexOf(observer);

    if (observerIndex === -1) {
      this.observers.push(observer);
    }
  }

  public detach(observer: Observer): void {
    const observerIndex: number = this.observers.indexOf(observer);

    if (observerIndex !== -1) {
      this.observers.splice(observerIndex, 1);
    }
  }

  public abstract notify(state?: GateState): void;
}
