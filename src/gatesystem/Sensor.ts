import Subject from '@/gatesystem/Subject';

export default class Sensor extends Subject {
  public notify(): void {
    this.observers.forEach(
      (observer) => observer.update(this),
    );
  }

  // eslint-disable-next-line class-methods-use-this
  public hasCar(): boolean {
    // TODO: add a car's presence checking
    return false;
  }
}
