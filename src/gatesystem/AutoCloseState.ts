import GateState from '@/gatesystem/GateState';

export default abstract class AutoCloseState extends GateState {
  private closeTimer!: ReturnType<typeof setTimeout>;

  public initClosingTimer(): void {
    this.closeTimer = setTimeout(
      () => this.timerHandler(),
      this.gateSystem.getClosingDelay(),
    );
  }

  private timerHandler(): void {
    if (this.gateSystem.sensor.hasCar()) {
      this.initClosingTimer();
    } else {
      this.setClosingState();
    }
  }

  public resetClosingTimer(): void {
    clearTimeout(this.closeTimer);
    this.closeTimer = 0;
  }

  private setClosingState(): void {
    this.gateSystem.setState(
      new this.gateSystem.states.ClosingState(this.gateSystem),
    );
  }
}
