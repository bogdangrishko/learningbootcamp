import MovementState from '@/gatesystem/MovementState';

export default class OpeningState extends MovementState {
  setNextState(): void {
    this.gateSystem.setState(
      new this.gateSystem.states.OpenedState(this.gateSystem),
    );
  }

  setOppositeState(): void {
    this.gateSystem.setState(
      new this.gateSystem.states.ClosingState(this.gateSystem),
    );
  }
}
