import { GateSystem } from '@/gatesystem/types';
import AutoCloseState from '@/gatesystem/AutoCloseState';

export default abstract class MovementState extends AutoCloseState {
  private movementTimer!: ReturnType<typeof setTimeout>;

  constructor(gateSystem: GateSystem) {
    super(gateSystem);

    this.initMovementTimer();
  }

  private initMovementTimer(): void {
    this.movementTimer = setTimeout(
      () => this.setNextState(),
      this.gateSystem.getMovementTime(),
    );
  }

  protected resetMovementTimer(): void {
    clearTimeout(this.movementTimer);
    this.movementTimer = 0;
  }

  protected abstract setNextState(): void;

  protected abstract setOppositeState(): void;

  public handleMainButtonPress(): void {
    if (this.movementTimer) {
      this.resetMovementTimer();
      this.initClosingTimer();
    } else {
      this.resetClosingTimer();
      this.setOppositeState();
    }
  }
}
