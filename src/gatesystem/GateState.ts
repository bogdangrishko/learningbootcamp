/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable class-methods-use-this */
import { GateState as GateStateType, GateSystem, Sensor } from '@/gatesystem/types';

export default abstract class GateState implements GateStateType {
  protected gateSystem: GateSystem;

  constructor(gateSystem: GateSystem) {
    this.gateSystem = gateSystem;
  }

  public abstract handleMainButtonPress(): void;

  public update(sensor: Sensor, data?: unknown): void {}
}
