import { GateSystem } from '@/gatesystem/types';
import AutoCloseState from '@/gatesystem/AutoCloseState';

export default class OpenedState extends AutoCloseState {
  constructor(gateSystem: GateSystem) {
    super(gateSystem);

    this.initClosingTimer();
  }

  public handleMainButtonPress(): void {
    this.resetClosingTimer();

    if (this.gateSystem.sensor.hasCar()) {
      this.initClosingTimer();
    } else {
      this.gateSystem.setState(
        new this.gateSystem.states.ClosingState(this.gateSystem),
      );
    }
  }
}
