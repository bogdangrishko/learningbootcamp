import { GateState } from '@/gatesystem/types';
import Subject from '@/gatesystem/Subject';

export default class EventManager extends Subject {
  public notify(state: GateState): void {
    this.observers.forEach(
      (observer) => observer.update(this, state),
    );
  }
}
