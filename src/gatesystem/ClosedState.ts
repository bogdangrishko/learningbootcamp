import GateState from '@/gatesystem/GateState';

export default class ClosedState extends GateState {
  public handleMainButtonPress(): void {
    this.gateSystem.setState(
      new this.gateSystem.states.OpeningState(this.gateSystem),
    );
  }
}
