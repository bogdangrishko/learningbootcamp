/* eslint-disable no-use-before-define */
export interface Subject {
  attach(observer: Observer): void;

  detach(observer: Observer): void;

  notify(data?: unknown): void;
}

export interface Observer {
  update(subject: Subject, data?: unknown): void;
}

export interface Sensor extends Subject {
  hasCar(): boolean;
}

export interface GateState extends Observer {
  handleMainButtonPress(): void;
}

export interface AutoCloseState extends GateState {
  initClosingTimer(): void;

  resetClosingTimer(): void;
}

export interface GateStateConstructor {
  new (gateSystem: GateSystem): GateState;
}

export interface StateList {
  ClosedState: GateStateConstructor;
  OpenedState: GateStateConstructor;
  ClosingState: GateStateConstructor;
  OpeningState: GateStateConstructor;
}

export interface GateSystem {
  states: StateList;

  sensor: Sensor;

  eventManager: Subject;

  setState(state: GateState): void;

  handleMainButtonPress(): void;

  getClosingDelay(): number;

  setClosingDelay(delay: number): void;

  getMovementTime(): number;

  setMovementTime(time: number): void;
}

export interface Data {
  gateSystem: GateSystem;
  closingDelay: number;
  movementTime: number;
  currentState: GateState | null;
  logs: Log[];
}

export interface Log {
  id: number;
  data: string;
}
