/* eslint-disable import/prefer-default-export */
export function debounce(func: (...args: any[]) => void, timeout = 300): () => void {
  let timer: ReturnType<typeof setTimeout>;

  return (...args: any[]) => {
    clearTimeout(timer);

    timer = setTimeout(() => {
      func(...args as []);
    }, timeout);
  };
}
