import { GateSystem as GateSystemType, GateState } from '@/gatesystem/types';
import ClosedState from '@/gatesystem/ClosedState';
import OpenedState from '@/gatesystem/OpenedState';
import ClosingState from '@/gatesystem/ClosingState';
import OpeningState from '@/gatesystem/OpeningState';
import Sensor from '@/gatesystem/Sensor';
import EventManager from '@/gatesystem/EventManager';

export default class GateSystem implements GateSystemType {
  public states = {
    ClosedState,
    OpenedState,
    ClosingState,
    OpeningState,
  };

  public sensor: Sensor = new Sensor();

  public eventManager: EventManager = new EventManager();

  private currentState: GateState = new this.states.ClosedState(this);

  private closingDelay = 3000;

  private movementTime = 3000;

  private static instance: GateSystem;

  public setState(state: GateState): void {
    this.currentState = state;

    this.eventManager.notify(state);
  }

  public handleMainButtonPress(): void {
    this.currentState.handleMainButtonPress();
  }

  public getClosingDelay(): number {
    return this.closingDelay;
  }

  public setClosingDelay(delay: number): void {
    this.closingDelay = delay;
  }

  public getMovementTime(): number {
    return this.movementTime;
  }

  public setMovementTime(time: number): void {
    this.movementTime = time;
  }

  public static getInstance(): GateSystem {
    if (!this.instance) {
      this.instance = new GateSystem();
    }

    return this.instance;
  }
}
