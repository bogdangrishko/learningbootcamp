import { GateSystem, Sensor } from '@/gatesystem/types';
import MovementState from '@/gatesystem/MovementState';

export default class ClosingState extends MovementState {
  constructor(gateSystem: GateSystem) {
    super(gateSystem);

    gateSystem.sensor.attach(this);
  }

  setNextState(): void {
    this.gateSystem.setState(
      new this.gateSystem.states.ClosedState(this.gateSystem),
    );
  }

  setOppositeState(): void {
    this.gateSystem.setState(
      new this.gateSystem.states.OpeningState(this.gateSystem),
    );
  }

  public update(sensor: Sensor): void {
    this.resetMovementTimer();
    this.setOppositeState();

    sensor.detach(this);
  }
}
